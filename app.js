const ws = require('ws')
const express = require('express')
const app = express()

const server = new ws.Server({
  port: 9090
})
const clients = []

app.use(express.static(__dirname + '/src'))


const EVENT_TYPES = {
  INIT_GAME: 'INIT_GAME',
  UPDATE_GAME: 'UPDATE_GAME',
  CREATE_PLAYER: 'CREATE_PLAYER',
  UPDATE_PLAYER: 'UPDATE_PLAYER',
  DELETE_PLAYER: 'DELETE_PLAYER',
  READ_PLAYER:   'READ_PLAYER'
}

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html')
})

server.on('connection', socket => {
  socket.on('message', msg => {
    const data = JSON.parse(msg)
    const { type, payload } = data

    if (type === EVENT_TYPES.CREATE_PLAYER) {
      console.log('CREATE_PLAYER')
      socket.player = payload.player
      console.log(socket.player.id)
      socket.send(JSON.stringify({
        type: EVENT_TYPES.INIT_GAME,
        payload: {
          players: Array.from(server.clients).filter(el => el.player).map(el => el.player).filter(el => el.id != socket.player.id)
        }
      }))

      server.clients.forEach(el => {
        if (el == socket) return

        el.send(JSON.stringify({
          type: EVENT_TYPES.CREATE_PLAYER,
          payload: {
            player: socket.player
          }
        }))
      })
    }

    if (type === EVENT_TYPES.UPDATE_PLAYER) {
      // console.log('UPDATE_PLAYER', socket.player.name)
      socket.player = payload.player

      server.clients.forEach(el => {
        if (el == socket) return

        el.send(JSON.stringify({
          type: 'UPDATE_PLAYER',
          payload: {
            player: socket.player
          }
        }))
      })
    }
  })

  console.log(clients.map(socket => socket.player))

  socket.on('close', () => {
    console.log('DISCONNECT_PLAYER', socket.player.name)
    server.clients.forEach(el => {
      if (el == socket) return
      el.send(JSON.stringify({
        type: EVENT_TYPES.DELETE_PLAYER,
        payload: {
          id: socket.player.id
        }
      }))
    })
  })
})

app.listen(6776, () => console.log('SERVER STARTED'))
