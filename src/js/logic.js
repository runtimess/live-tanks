(document => {
	class Tank {
		constructor(x, y, side, headRadius, speed = 5, direction = 'RIGHT') {
			// Position
			this.x = x
			this.y = y
			this.id = Math.floor(Math.random() * 1111111)
			this.name = 'Tank ' + this.id
			// Size
			this.side = side

			this.speed = speed

			// Bullet is settings
			this.bulletRadius = 20
			this.bulletDamage = 200
			this.bulletSpeed = 10
			this.bulletIntervalValue = 300
			this.bulletInterval = undefined

			this.health = 1000

			// Colors
			this.hornetColor = '#ccc'
			this.headColor = '#999'
			this.barrelColor = '#555'
			this.bulletColor = '#000'

			this.headRadius = headRadius
			this.barrelWidth = 20
			this.barrelLength = 30

			this.direction = direction
			this.bullets = []
			this.dead = false
		}

		shootBullet() {
			if (this.bulletInterval) return

			this.bulletInterval = setTimeout(() => {

				this.bulletInterval = undefined
			
			}, this.bulletIntervalValue)

			const bullet = this.computeBullet()

			this.bullets.push(bullet)
		}

		computeBullet() {
			const { bulletDamage: damage, bulletRadius: radius, bulletSpeed: speed } = this

			const { direction, x, y, side } = this

			const { id: tankId } = this

			const fromSideBullet = []

			if (direction === 'UP') {

				fromSideBullet[0] = x + side / 2
				fromSideBullet[1] = y - radius * 2

			}

			if (direction === 'DOWN') {

				fromSideBullet[0] = x + side / 2
				fromSideBullet[1] = y + side + radius * 2

			}

			if (direction === 'LEFT') {

				fromSideBullet[0] = x - radius * 2
				fromSideBullet[1] = y + side / 2

			}

			if (direction === 'RIGHT') {

				fromSideBullet[0] = x + side + radius * 2
				fromSideBullet[1] = y + side / 2

			}

			return {
				speed, 
				damage,
				tankId, 
				r: radius, 
				direction, 
				x: fromSideBullet[0], 
				y: fromSideBullet[1]
			}
		}
	}


	class Obstacle {
		constructor(x, y, side) {
			this.x = x
			this.y = y
			this.side = side
		}
	}
	// class LiveConnector {
	// 	constructor(host = 'localhost', port = 8080) {
	// 		const this._socket = new WebSocket(`ws://${host}:${port}`)
	// 	}
	// }
	class GameManager {
		constructor(canvas, players, obstacles) {
			this._players = players
			this._currentPlayerIndex = 0
			this._obstacles = obstacles
			this._cvs = canvas
			this._ctx = canvas.getContext('2d')
			this._opts = {
				keyboard: {
					UP: false,
					DOWN: false,
					LEFT: false,
					RIGHT: false,
					DEFAULT: 'RIGHT'
				}
			}
		}

		setup(document) {
			document.onkeydown = this.onKeyDownHandler.bind(this)
			document.onkeyup = this.onKeyUpHandler.bind(this)
			const newGameDialog = document.querySelector('#new-game-form')
			this._cvs.width = window.innerWidth
			this._cvs.height = window.innerHeight

			// Socket connection
			const socket = new WebSocket('ws://localhost:9090')

			socket.onopen = meta => {
				newGameDialog.querySelector('#new-game-btn').onclick = this.onNewGame.bind(this, socket)
			}

			socket.onmessage = meta => {
				const data = JSON.parse(meta.data)
				const { type, payload } = data
				// console.log(type, payload)
				if (type === 'INIT_GAME') {
					payload.players.forEach(el => this.createPlayer(el))
					newGameDialog.closest('.ui-wrapper').style.display = 'none'
					this.update.call(this, socket)
				}

				if (type === 'CREATE_PLAYER') {
					this.createPlayer(payload.player)
				}

				if (type === 'UPDATE_PLAYER') {
					this.updatePlayer(payload.player)
				}

				if (type === 'DELETE_PLAYER') {
					this.deletePlayer(payload.id)
				}
			}

			socket.onerror = meta => {

			}

		}


		update(socket) {
			let headDirMeta
			const players = this._players
			const obstacles = this._obstacles
			const currentPlayer = players[this._currentPlayerIndex]
			const ctx = this._ctx
			const cvs = this._cvs

			if (this._opts.keyboard.UP) {
				currentPlayer.y -= currentPlayer.speed
				currentPlayer.y = currentPlayer.y <= 0 ? 0 : currentPlayer.y
			}

			if (this._opts.keyboard.DOWN) {
				currentPlayer.y += currentPlayer.speed
				currentPlayer.y = currentPlayer.y + currentPlayer.side >= cvs.height ? cvs.height - currentPlayer.side : currentPlayer.y
			}

			if (this._opts.keyboard.LEFT) {
				currentPlayer.x -= currentPlayer.speed
				currentPlayer.x = currentPlayer.x <= 0 ? 0 : currentPlayer.x
			}

			if (this._opts.keyboard.RIGHT) {
				currentPlayer.x += currentPlayer.speed
				currentPlayer.x = currentPlayer.x + currentPlayer.side >= cvs.width ? cvs.width - currentPlayer.side : currentPlayer.x
			}

			ctx.clearRect(0, 0, cvs.width, cvs.height)

			// Obstacles drawing
			for (let i in obstacles) {
				const obstacle = obstacles[i]

				ctx.fillRect(obstacle.x, obstacle.y, obstacle.side, obstacle.side)
			}

			// Loop for drawing all players
			for (let i in players) {

				const player = players[i]
				
				for (let i in player.bullets) {
					const bullet = player.bullets[i]

					if (bullet.direction === 'UP')
						bullet.y -= bullet.speed
					if (bullet.direction === 'DOWN')
						bullet.y += bullet.speed
					if (bullet.direction === 'LEFT')
						bullet.x -= bullet.speed
					if (bullet.direction === 'RIGHT')
						bullet.x += bullet.speed

					if (bullet.y >= cvs.height|| bullet.y <= 0 || bullet.x >= cvs.width || bullet.x <= 0)
						player.bullets[i].y = 10000

					// draw a bullet
					ctx.fillStyle = player.bulletColor
					ctx.beginPath()
					ctx.arc(bullet.x, bullet.y, bullet.r, 0, Math.PI * 2)
					ctx.fill()
					ctx.closePath()

					// Collision obstacles
					for (let j in obstacles) {
						const obstacle = obstacles[j]

						if (bullet.x >= obstacle.x && bullet.x <= obstacle.x + obstacle.side && bullet.y >= obstacle.y && bullet.y <= obstacle.y + obstacle.side) {
							obstacles.splice(obstacles.indexOf(obstacle), 1)
							player.bullets.splice(i, 1)
						}
					}

					// Collision players
					for (let p in players) {
						const player = players[p]

						if (bullet.x >= player.x && bullet.x <= player.x + player.side && bullet.y >= player.y && bullet.y <= player.y + player.side) {
							// gameManager.removePlayer(player)
							player.health -= bullet.damage
							if (player.health <= 0) {
								const killer = players.find(el => el.id === bullet.tankId)
								console.log(`${killer.name} killed ${player.name}`)
								// players[p].dead = true
								players[p].x = cvs.width + 500
								players[p].y = cvs.height + 500
								players[p].dead = true
								// players.splice(p, 1)
							}
							currentPlayer.bullets.splice(i, 1)
						}

					}
				}

				// if (player.dead)
				// Draw the hornet
				ctx.fillStyle = player.hornetColor
				ctx.fillRect(player.x, player.y, player.side, player.side)
				// Draw the head
				ctx.fillStyle = player.headColor
				ctx.beginPath()
				ctx.arc(player.x + player.side / 2, player.y + player.side / 2, player.headRadius,0,2*Math.PI)
				ctx.fill()
				ctx.closePath()
				// ctx.fillRect(, player.headSide, player.headSide)
				// Draw the gun 
				ctx.fillStyle = player.barrelColor
				if (player.direction === 'UP') {
					headDirMeta = [player.x + player.side / 2 - player.barrelWidth / 2, (player.y - player.barrelLength + player.side / 2), player.barrelWidth, player.barrelLength]
				}
				if (player.direction === 'DOWN') {
					headDirMeta = [player.x + player.side / 2 - player.barrelWidth / 2, (player.y + player.side / 2), player.barrelWidth, player.barrelLength]
				}
				if (player.direction === 'LEFT') {
					headDirMeta = [player.x - player.barrelLength + player.side / 2, (player.y + player.side / 2) - player.barrelWidth / 2, player.barrelLength, player.barrelWidth]
				}
				if (player.direction === 'RIGHT') {
					headDirMeta = [player.x + player.side / 2, (player.y + player.side / 2) - player.barrelWidth / 2, player.barrelLength, player.barrelWidth]
				}

				const [headX, headY, bW, bH] = headDirMeta
				ctx.fillRect(headX, headY, bW, bH)
				ctx.fillStyle = 'black'
				ctx.font = '20px Play'
				ctx.textAlign = 'center'
				ctx.fillText(player.name, player.x + player.side / 2, player.y - 30)

				ctx.fillStyle = 'lime'
				ctx.fillRect(player.x - player.health / 10 / 2 + player.side / 2, player.y - 20, player.health / 10, 10)
			}

			socket.send(JSON.stringify({
				type: 'UPDATE_PLAYER',
				payload: {
					player: currentPlayer
				}
			}))
			// Updater
			requestAnimationFrame(this.update.bind(this, socket))
		}

		createPlayer(player) {
			console.log('NEW_PLAYER_JOIN', player.name, player.direction)
			const newPlayer = new Tank(player.x, player.y, player.side, player.headRadius, player.speed, player.direction)
			newPlayer.id = player.id
			newPlayer.name = player.name
			this._players.push(newPlayer)
		}

		deletePlayer(id) {
			const idx = this._players.findIndex(el => el.id == id)
			this._players.splice(idx, 1)
		}

		updatePlayer(player) {
			const id = player.id
			const tank = this._players.find(el => el.id === id)
			tank.x = player.x
			tank.y = player.y
			tank.side = player.side
			tank.headRadius = player.headRadius
			tank.direction = player.direction
			tank.bullets = player.bullets
			// tank.health = player.health
		}

		getPlayer(index) {
			return this._players[index]
		}

		get players() {
			return this._players
		}

		get obstacles() {
			return this._obstacles
		}

		get currentPlayerIndex() {
			return this._currentPlayerIndex
		}

		removeObstacle() {

		}

		createObstacle() {

		}

		onNewGame(socket, e) {
			const form = e.currentTarget.closest('.form')
			const nickname = form.querySelector('input[name="nickname"]').value.trim()
			const barrelColor = form.querySelector('input[name="barrel-color"]').value
			const headColor = form.querySelector('input[name="head-color"]').value
			const hornetColor = form.querySelector('input[name="hornet-color"]').value
			const player = this._players[this._currentPlayerIndex]
			player.name = nickname
			socket.send(JSON.stringify({
				type: 'CREATE_PLAYER',
				payload: { player }
			}))
			console.log(nickname, barrelColor, headColor, hornetColor)
		}

		onKeyDownHandler(e) {
			const code = e.keyCode
			const currentPlayer = this._players[this._currentPlayerIndex]
			
			if (currentPlayer.dead) return

			if (code === 38) {
				currentPlayer.direction = 'UP'
				this._opts.keyboard.UP = true
			}

			if (code === 40) {
				currentPlayer.direction = 'DOWN'
				this._opts.keyboard.DOWN = true
			}

			if (code === 37) {
				currentPlayer.direction = 'LEFT'
				this._opts.keyboard.LEFT = true
			}

			if (code === 39) {
				currentPlayer.direction = 'RIGHT'
				this._opts.keyboard.RIGHT = true
			}

			if (code === 32) {
				this._opts.keyboard.SPACE = true
				currentPlayer.shootBullet()
			}
		}

		onKeyUpHandler(e) {
			const code = e.keyCode

			if (code === 38)
				this._opts.keyboard.UP = false

			if (code === 40)
				this._opts.keyboard.DOWN = false

			if (code === 37)
				this._opts.keyboard.LEFT = false

			if (code === 39)
				this._opts.keyboard.RIGHT = false

			if (code === 32)
				this._opts.keyboard.SPACE = false
			
		}
	}

	new GameManager(document.querySelector('#cvs'), [new Tank(100, 100, 50, 20)], [new Obstacle(0, 0, 100)]).setup(document)
})(document)